package hgn.cours01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PrincipaleTest {
    @Test
    public void FizzBuzzNormalNumbers() {

        Principale fb = new Principale();
        Assertions.assertEquals("1", fb.convert(1));
        Assertions.assertEquals("2", fb.convert(2));
        Assertions.assertEquals("Fizz", fb.convert(3));
        Assertions.assertEquals("4", fb.convert(4));
    }

    @Test
    public void FizzBuzzThreeNumbers() {

        Principale fb = new Principale();
        Assertions.assertEquals("Fizz", fb.convert(3));
    }

    @Test
    public void FizzBuzzFiveNumbers() {

        Principale fb = new Principale();
        Assertions.assertEquals("Buzz", fb.convert(5));
    }

    @Test
    public void FizzBuzzThreeAndFiveNumbers() {

        Principale fb = new Principale();
        Assertions.assertEquals("Buzz", fb.convert(5));
    }
}