package hgn.cours01;

import static java.lang.System.Logger.Level.DEBUG;

public class Principale {
    private static final System.Logger LOGGER = System.getLogger("c.f.b.DefaultLogger");
    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            System.out.println(convert(i));
        }
        transport();

    }


    public static String convert(int fizzBuzz) {
        if (fizzBuzz % 15 == 0) {
            return "FizzBuzz";
        }
        if (fizzBuzz % 3 == 0) {
            return "Fizz";
        }
        if (fizzBuzz % 5 == 0) {
            return "Buzz";
        }
        return String.valueOf(fizzBuzz);
    }

    public static String transport() {
        hgn.cours01.transport.Voiture voiture = new hgn.cours01.transport.Voiture();
        System.out.println("Oui");
        LOGGER.log(DEBUG, "Yes !");
        System.out.println(voiture.toString());

        voiture.accelerer(110);
        System.out.println(voiture);

        voiture.decelerer(20);
        System.out.println(voiture);

        voiture.freiner();
        System.out.println(voiture);
        return "OK";
    }
}
